package main

import (
	"net/http"
	"log"
	"github.com/gorilla/websocket"
	"os"
)

func main() {
	port := os.Getenv("SEABATTLE_PORT")
	log.Print("Start seabattleserver on port ", port)
	http.HandleFunc("/ws/", wsHandler)
	log.Fatal(http.ListenAndServe(":" + port, nil))
}

var upgrader = websocket.Upgrader{} // use default options

var opponent = make(chan *websocket.Conn)

func wsHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("upgrade:", err)
		return
	}
	type message [2]interface{}
	select {
	case opp := <-opponent:
		// found a opponent
		log.Print("Second player connected")
		conn.WriteJSON(message{"firstTurn", false})
		go forward(conn, opp)
		go forward(opp, conn)
	default:
		// wait for another player
		log.Print("First player connected")
		conn.WriteJSON(message{"firstTurn", true})
		opponent <- conn
		// TODO: handle disconnected opponent
	}
}

func forward(a, b *websocket.Conn) {
	defer log.Print("Connection closed")
	defer a.Close()
	defer b.Close()
	for {
		messageType, message, err := a.ReadMessage()
		if err != nil {
			return
		}
		// Log messages
		println(a.RemoteAddr().String(), string(message))
		err = b.WriteMessage(messageType, message)
		if err != nil {
			return
		}
	}
}
